# Bookstore RESTful API

<h2>Project setup</h2>
<h3>Install Require</h3>
- Install Java JDK8 <br/>
- Install maven version 3.3+ <br/>
- Install docker (https://www.docker.com/get-started)

<h3>Checkout project</h3>
- <code>git clone https://gitlab.com/metha.kth/bookstore-restful-api.git</code>

<h3>Run docker bookstoredb</h3>
- Run <code>docker-compose up -d</code> in docker folder<br/>
- Read log in container <code>docker logs -f bookstoredb</code>  

<h3>Start api server </h3>
- Compile <code>$ mvn clean compile</code> at project directory <br/>
- Run <code>$ mvn spring-boot:run</code> at project directory 

<h3>Postman</h3>
- Import these files.(in postman folder)<br/>
	1. Bookstore Api.postman_collection.json<br/>
	2. Bookstore Environment.postman_environment.json

<h3>OR</h3>

<h3>cURL</h3>

- Create user <br/>
	<code>curl -X POST "http://localhost:8090/bookstoreapi/users" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date_of_birth\": \"2019-12-02T21:28:14.969Z\", \"name\": \"john\", \"password\": \"password\", \"surname\": \"doe\", \"username\": \"john.doe\"}"</code>
	
- Login <br/>
	<code>curl -X POST http://localhost:8090/bookstoreapi/oauth/token -H 'Authorization: Basic Y2xpZW50SWQ6Y2xpZW50U2VjcmV0' -F grant_type=password -F username=john.doe -F password=password</code>


<h3>Swagger UI</h3>
- url http://localhost:8090/bookstoreapi/swagger-ui.html<br/>
    NOTE. Authorization value = <code>bearer {{access_token}}</code>



