package com.bookstore.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.common.DateUtil;
import com.bookstore.common.ErrorCode;
import com.bookstore.entity.OrderEntity;
import com.bookstore.entity.UserEntity;
import com.bookstore.exception.BookstoreException;
import com.bookstore.repository.OrderRepository;
import com.bookstore.repository.UserRepository;
import com.bookstore.request.UserDataRequest;
import com.bookstore.response.UserDataResponse;

@Service
@Transactional
public class UsersService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository;
		
	@Autowired
	AuthService authService;

	public boolean delete(){
		String userName = authService.getTokenData().getUserName();
		Optional<UserEntity> optUser = userRepository.findByUsername(userName);
		if(optUser.isPresent()) {
			List<OrderEntity> orderList = orderRepository.findIdByUserId(optUser.get().getId());
			orderRepository.deleteAll(orderList);
			userRepository.deleteById(optUser.get().getId());
			return true;
		} else {
			return false;
		}
	}

	public Long createUser(UserDataRequest request){
		Optional<UserEntity> optUser = userRepository.findByUsername(request.getUsername());
		if(optUser.isPresent()) {
			throw new BookstoreException("Username is already.", ErrorCode.DUPLICATE_DATA);
		}
		UserEntity user = new UserEntity();
		user.setUsername(request.getUsername());
		String password = String.format("%1$s%2$s", "{bcrypt}",  new BCryptPasswordEncoder().encode(request.getPassword()));
		user.setPassword(password);
		user.setDateOfBirth(request.getDate_of_birth());
		user.setName(request.getName());
		user.setSurname(request.getSurname());
		user = userRepository.saveAndFlush(user);
		return user.getId();
	}

	public UserDataResponse getUser() {
		String userName = authService.getTokenData().getUserName();
		Optional<UserEntity> optUser = userRepository.findByUsername(userName);
		if(optUser.isPresent()) {
			UserDataResponse response = new UserDataResponse();
			response.setName(optUser.get().getName());
			response.setSurname(optUser.get().getSurname());
			response.setDate_of_birth(DateUtil.formatDate(optUser.get().getDateOfBirth(), "dd/MM/yyyy"));
			response.setBooks(orderRepository.findBookIdByUserId(optUser.get().getId()));
			return response;
		}
		return null;
	}

}