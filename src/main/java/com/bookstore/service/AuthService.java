package com.bookstore.service;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import lombok.Data;

@Component
public class AuthService implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private TokenStore tokenServices;

	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public TokenData getTokenData() {
		TokenData tokenData = null;
		Authentication userAuthentication = getAuthentication();
		if (!ObjectUtils.isEmpty(userAuthentication.getDetails()) && (userAuthentication.getDetails() instanceof OAuth2AuthenticationDetails)) {
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails) userAuthentication.getDetails();
			String jwtToken = oAuth2AuthenticationDetails.getTokenValue();
			if (!ObjectUtils.isEmpty(jwtToken)) {
				Map<String, Object> details = tokenServices.readAccessToken(oAuth2AuthenticationDetails.getTokenValue()).getAdditionalInformation();
				tokenData = new TokenData(details);
			}
		}
		return tokenData;

	}

	public String getToken() {
		return (OAuth2AuthenticationDetails.class
			.cast(SecurityContextHolder.getContext().getAuthentication().getDetails())).getTokenValue();
	}

	@Data
	public class TokenData {
		private static final String OAUTH_USER_NAME = "user_name";
		private String userName;

		TokenData(Map<String, Object> details) {
			this.userName = (String) details.get(OAUTH_USER_NAME);
		}

	}

}
