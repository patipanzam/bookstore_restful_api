package com.bookstore.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.common.ErrorCode;
import com.bookstore.exception.BookstoreException;
import com.bookstore.request.UserDataRequest;
import com.bookstore.request.UserOrderBookRequest;
import com.bookstore.response.UserDataResponse;
import com.bookstore.response.UserOrderBookResponse;
import com.bookstore.service.OrderService;
import com.bookstore.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	OrderService orderService;
	
	@GetMapping(headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<UserDataResponse> getUser() {
		UserDataResponse response = usersService.getUser();
		if(response != null) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		throw new BookstoreException("User not found.", ErrorCode.NO_DATA_FOUND);
	}

	@PostMapping
	public ResponseEntity<?> createUser(
		@Valid @RequestBody  UserDataRequest request
	) {
		Long id = usersService.createUser(request);
		if( id != null) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			throw new BookstoreException("Can't create user.", ErrorCode.CANNOT_SAVE_DATA);
		}
	}

	@DeleteMapping(headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<?> deleteUser() {
		boolean deleteSuccess = usersService.delete();
		if(deleteSuccess) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		throw new BookstoreException("User not found.", ErrorCode.NO_DATA_FOUND);
	}
	
	@PostMapping(value = "/order", headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<UserOrderBookResponse> orderBook(
		@RequestBody UserOrderBookRequest request
	) {
		UserOrderBookResponse response = orderService.save(request);
		if(response != null) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		throw new BookstoreException("Can't save order.", ErrorCode.CANNOT_SAVE_DATA);
	}
}
