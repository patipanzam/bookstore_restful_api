package com.bookstore.common;

public class ErrorCode {
	public static final String UNKNOWN_ERROR = "0";
	public static final String NO_DATA_FOUND = "1";
	public static final String CANNOT_SAVE_DATA = "2";
	public static final String CANNOT_LOAD_DATA = "3";
	public static final String DUPLICATE_DATA = "4";
	public static final String UPDATE_BOOK_FAIL = "5";
	public static final String WHEN_CALL_API = "6";
}

