package com.bookstore.exception;

import java.util.Date;

import lombok.Data;

@Data
public class ApiError {
	private Date timestamp;
	private String code;
	private String message;
	private String debugMessage;

	private ApiError(){
		this.timestamp = new Date();
	}

	ApiError(String code){
		this();
		this.code = code;
	}

	ApiError(String code, String message, Throwable ex){
		this();
		this.code = code;
		this.message = message;
		this.debugMessage = ex.getLocalizedMessage();
	}
	
}