package com.bookstore.response;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class UserOrderBookResponse {
	private BigDecimal price;
}
